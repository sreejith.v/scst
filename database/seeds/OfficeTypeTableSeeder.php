<?php

use Illuminate\Database\Seeder;

class OfficeTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('officeType')->insert([
            'id' => 1,
            'name' => 'SCDO',
            'status' => 1,
        ]);
        DB::table('officeType')->insert([
            'id' => 2,
            'name' => 'DDO',
            'status' => 1,
        ]);
        DB::table('officeType')->insert([
            'id' => 3,
            'name' => 'DTDO',
            'status' => 1,
        ]);
        DB::table('officeType')->insert([
            'id' => 4,
            'name' => 'Directorate-SC',
            'status' => 1,
        ]);
        DB::table('officeType')->insert([
            'id' => 5,
            'name' => 'Directorate-ST',
            'status' => 1,
        ]);
        DB::table('officeType')->insert([
            'id' => 6,
            'name' => 'Minister',
            'status' => 1,
        ]);
    }
}
