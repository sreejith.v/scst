<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('publicProfile', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('user_id')->unsigned()->default(0);
            $table->Integer('addressBook_id')->unsigned()->default(0);
            $table->Integer('sickDetails_id')->unsigned()->default(0);
            $table->string('patient_name',255)->default('');
            $table->string('patient_email',255)->default('');
            $table->string('patient_mobile',15)->default('');
            $table->string('aadhar_number',40)->default('');
            $table->string('occupation',255)->default('');
            $table->Integer('age')->unsigned()->default(0);
            $table->double('annual_income', 15, 2)->default(0);
            $table->string('bank_code',20)->default('');
            $table->string('ifsc_code',20)->default('');
            $table->string('bank_account_no',100)->default('');
            $table->string('caste_id',10)->default('');
            $table->dateTime('caste_certificate_date');
            $table->string('caste_certificate_number',200)->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('publicProfile');
    }
}
