<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addressBook', function (Blueprint $table) {
            $table->increments('id');
            $table->text('address');
            $table->Integer('district_id')->unsigned()->default(0);
            $table->Integer('block_id')->unsigned()->default(0);
            $table->Integer('pincode')->unsigned()->default(0);
            $table->Integer('constituency_id')->unsigned()->default(0);
            $table->Integer('localbody_type')->unsigned()->default(0);
            $table->Integer('localbody_id')->unsigned()->default(0);
            $table->string('applicant_name',255)->default('');
            $table->Integer('applicant_patient_relationship')->unsigned()->default(0);
            $table->Integer('Recommending_authority')->unsigned()->default(0);
            $table->Integer('Recommendor_id')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addressBook');
    }
}
