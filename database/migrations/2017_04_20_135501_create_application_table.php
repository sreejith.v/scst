<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('user_id')->unsigned()->default(0);
            $table->Integer('applicationType_id')->unsigned()->default(0);
            $table->Integer('district_id')->unsigned()->default(0);
            $table->string('application_number',255)->default('');
            $table->Integer('application_status')->unsigned()->default(0);
            $table->Integer('present_flow_staff_id')->unsigned()->default(0);
            $table->dateTime('submitted_date');
            $table->Integer('office_id')->unsigned()->default(0);
            $table->dateTime('putup_datetime');
            $table->dateTime('approval_date');
            $table->Integer('appoval_user_id')->unsigned()->default(0);
            $table->tinyInteger('status')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('application');
    }
}
