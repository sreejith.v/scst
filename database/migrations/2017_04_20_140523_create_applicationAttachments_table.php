<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicationAttachments', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('application_id')->unsigned()->default(0);
            $table->Integer('attachmentsType_id')->unsigned()->default(0);
            $table->string('attachment_file',255)->default('');
            $table->dateTime('attachment_date');
            $table->tinyInteger('status')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applicationAttachments');
    }
}
