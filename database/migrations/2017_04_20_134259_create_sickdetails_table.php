<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSickdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sickDetails', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('sick_name_id')->unsigned()->default(0);
            $table->dateTime('treatment_started_date');
            $table->double('amount_spent_already', 15, 2)->default(0);
            $table->string('hospital_name',255)->default('');
            $table->double('balance_amount_required', 15, 2)->default(0);
            $table->tinyInteger('funding_received_already')->unsigned()->default(0);
            $table->Integer('dept_funded_already')->unsigned()->default(0);
            $table->double('relief_amount_received_already', 15, 2)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sickDetails');
    }
}
