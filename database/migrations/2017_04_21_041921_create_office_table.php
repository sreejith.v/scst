<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office', function (Blueprint $table) {
            $table->increments('id');
            $table->Integer('district_id')->unsigned()->default(0);
            $table->Integer('officeType_id')->unsigned()->default(0);
            $table->string('name',255)->default('');
            $table->string('code',25)->default('');
            $table->string('email',255)->default('');
            $table->string('phone',20)->default('');
            $table->string('pincode',10)->default('');
            $table->tinyInteger('status')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('office');
    }
}
