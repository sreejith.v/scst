<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\AdminLoginFormRequest;
//use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\User;
use DB;
use Collective\Html;
///use App\Binding\UserTypePrivileges\UserTypePrivilegesRepository as userTypePrivilageRepo;
class AdminLoginController extends Controller {

	/**          
	 * Display login form to enter credentials.
	 *
	 * @return Response
	 */

    public function __construct(){

     //   $this->user_type_privilage = $user_type_privilage;
    }
	public function index()
	{
;
            if(Auth::check()){

                if (Auth::user()->role == '0')
                {
                    Auth::logout();
                    return redirect('admin/login')->withInput(Input::except('password'))->withErrors(['error' => 'You are not authorised to login here...! ',]);
                }

            }
            return view('admin.login');
	}
        /**
	 * Check the login credentials.
	 *
	 * @return Response
	 */
        public function checkLogin(Request $request)
        {
//            dd($request->get('username').' '.$request->get('password'));

            $strAdminUsername   =   $request->get('username');
            $strAdminPassword   =   $request->get('password');
         //   $user_details  =   DB::table('users')->where('email', $strAdminUsername)->first();

            $user_details = User::where('email', $strAdminUsername)->first();

            if($user_details) {
                $user_role = $user_details->role;
            } else {
                $user_role = '';
            }

            $arrayUserInputs = ['email'  =>  $strAdminUsername,'password'    =>  $strAdminPassword];

            if(Auth::attempt($arrayUserInputs)){
                if(Auth::user()->status == '0'){
                    Auth::logout();
                    return redirect('admin/login')->withInput(Input::except('password'))->withErrors(['error' => 'Your account has been disabled...! ',]);
                }else {
                    if(Auth::user()->role == '0') {
                        Auth::logout();
                        return redirect('admin/login')->withInput(Input::except('password'))->withErrors(['error' => 'You are not authorised to login here.',]);
                    }
                    else {

// ---------- explode roles in an array -------------
                        $roles = explode(',' , Auth::user()->role);

// ---------- getting user type privilege based on role -------------
               //         $privilages = $this->user_type_privilage->getUserPrivilage($roles);

// ---------- converting user type privilege in to an array and adding to session -------------

                        /*
                        $userPrivilage = array();
                        foreach($privilages as $privilage){

                            array_push($userPrivilage,$privilage->privilege_id);
                        }
                        session()->push('privileges', $userPrivilage);
                        */

                        return redirect()->intended('admin/dashboard');
                    }

                }
            }
            else {
                return redirect('admin/login')->withInput(Input::except('password'))->withErrors(['error' => 'Incorrect username or password...! ',]);
            }
        }
        /**
	 * Logout from the admin area.
	 *
	 * @return Response
	 */
	public function logout()
    {
        session()->flush();
         Auth::logout();
        session()->forget('privileges');
         //Session::forget('admins');
         return view('admin.login');

    }

}
