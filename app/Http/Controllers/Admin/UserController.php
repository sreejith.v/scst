<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Binding\User\UserRepository as UserRepository;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use App\Binding\UserTypePrivileges\UserTypePrivilegesRepository as UserTypeRepo;

class UserController extends Controller
{
    //
    public function __construct(UserRepository $memberRepo,DepartmentRepository $departmentRepository,UserTypeRepo $userTypeRepo){

        $this->users = $memberRepo;
        $this->user_privilege = $userTypeRepo;

        $isSuperAdmin = strstr(Auth::user()->role,'1');
        $this->userAdd = 0;
        $this->userView = 0;

        if($isSuperAdmin == ''){

            $privileges = session('privileges');

            foreach($privileges[0] as $menu){

                if($menu == 8){
                    $this->userAdd = 1;
                }
                if($menu == 9){
                    $this->userView = 1;
                }
            }

        }else{
            $this->userAdd = 1;
            $this->userView = 1;
        }

    }

    public function index(){

        if($this->userAdd == 1 || $this->userView == 1){

            $entities = $this->users->userslist();
            return view('admin.users.lists')
                ->with('entities',$entities)
                ->with('userAdd',$this->userAdd)
                ->with('userView',$this->userView);

        }else{

            session()->flush();
            Auth::logout();
            session()->forget('privileges');
            return view('admin.login');

        }

    }

    public function create($id = NULL){

        if($this->userAdd == 1){

            $id = base64_decode($id);

            $departmentsArray = $this->department->getAll(1);
            $departments = array();
            $departments[null] = 'Select Department';
            foreach($departmentsArray as $department){

                $departments[$department->id] = $department->department_name;
            }
            $userTypes = $this->user_privilege->getAllUserType(1);

            $entity = null;
            if($id != NULL){

                $entity = $this->users->getById($id);
            }

            return view('admin.users.form')->with('entity',$entity)->with('departments',$departments)->with('userTypes',$userTypes);

        }else{

            session()->flush();
            Auth::logout();
            session()->forget('privileges');
            return view('admin.login');

        }


    }

    public function store(){

        $entityId = Input::get('entityId');

        $userTypes = Input::get('userType');

        $password = Input::get('password');

        $confirm_password = Input::get('confirm_password');

        if(is_null($entityId)) {
            if ($password == '') {
                return Redirect::back()->withInput()->withErrors("Please enter password.");
            }

            if ($confirm_password == '') {
                return Redirect::back()->withInput()->withErrors("Please enter confirm password.");
            }


            if (($password != '') && strlen($password) < 6) {
                return Redirect::back()->withInput()->withErrors("Password should contain atleast 6 characters.");
            }

            if (($password != '') && strlen($confirm_password) < 6) {
                return Redirect::back()->withInput()->withErrors("Confirm Password should contain atleast 6 characters.");
            }

            if ($password != $confirm_password) {
                return Redirect::back()->withInput()->withErrors("Password and Confirm Password are not matching.");
            }
        }

        $roles = 0;
        $userTypeIds = null;

        if($userTypes != null){

            foreach($userTypes as $userType){

                $userTypeIds .= $userType.',';
            }

            $roles = rtrim($userTypeIds, ",");
        }
        Input::merge(array('role' => $roles));

        //validation
        if(is_null($entityId)){
            $rules = array(
                'email' => 'required|unique:users',
            );
        }else{
            $rules = array(
                'email' => 'required|email',
            );
        }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) { //echo "validator if"; exit;

            return Redirect::back()
                ->withErrors($validator)
                ->withInput();

        } else { //echo "validator else"; exit;

            if (is_null($entityId))
            {

                $this->users->create(Input::all());
                return Redirect::to('/admin/users')->with('message', 'User added successfully.');
            }
            else
            {
                $this->users->update(Input::all());
                return Redirect::to('/admin/users')->with('message', 'User updated successfully.');
            }
        }

    }

    public function delete($id)
    {
        $id = base64_decode($id);

        $this->users->delete($id);
        return Redirect::to('/admin/users')->with('message', 'User Deleted successfully.');
    }

    public function changeStatus($id,$status){

        $id = base64_decode($id);
        $status = base64_decode($status);

        if($status == 1){
            $this->users->activate($id,$status);
            $msg = 'User Activated';
        }else{
            $this->users->deactivate($id,$status);
            $msg = 'User De-Activated';
        }

        return Redirect::to('/admin/users')->with('message', $msg);
    }


}
