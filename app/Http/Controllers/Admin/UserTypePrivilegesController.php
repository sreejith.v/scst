<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Binding\UserTypePrivileges\UserTypePrivilegesRepository as UserTypePrivilegesRepository;

use Illuminate\Support\Facades\Input;;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
class UserTypePrivilegesController extends Controller
{
    //

    public function __construct(UserTypePrivilegesRepository $userTypePrivileges){

        $this->user_privilege = $userTypePrivileges;

        $isSuperAdmin = strstr(Auth::user()->role,'1');
        $this->privilegeAdd = 0;

        if($isSuperAdmin == ''){

            $privileges = session('privileges');

            foreach($privileges[0] as $menu){

                if($menu == 10){
                    $this->privilegeAdd = 1;
                }
            }

        }else{
            $this->privilegeAdd = 1;
        }
    }

    public function index(){

        if($this->privilegeAdd == 1){

            $entities = $this->user_privilege->getAllUserType(1);

            return view('admin.user-privilege.lists')
                ->with('entities',$entities)
                ->with('privilegeAdd',$this->privilegeAdd);

        }else{

            session()->flush();
            Auth::logout();
            session()->forget('privileges');
            return view('admin.login');

        }
    }
    public function getPrivileges($userTypeId){

        if($this->privilegeAdd == 1){

                $entities = $this->user_privilege->getByUserTypeId($userTypeId);
                $userType = $this->user_privilege->getUserTypeById($userTypeId);

                $userPrivileges = array();
                $privilegesIds = array();

                foreach($entities as $key=>$entity){

                array_push($privilegesIds, $entity->privilege_id);

                    $userPrivileges[$key]['id'] = $entity->privilege_id;
                    $userPrivileges[$key]['title'] = $entity->title;
                    $userPrivileges[$key]['user_type_id'] = $entity->user_type_id;
                }


                 $privileges = $this->user_privilege->getPrivileges($privilegesIds);

                $i = sizeof($entities);

                foreach($privileges as $privilege){

                    $userPrivileges[$i]['id'] = $privilege->id;
                    $userPrivileges[$i]['title'] = $privilege->title;
                    $userPrivileges[$i]['user_type_id'] = null;

                    $i++;
                }

                $inputs = array('userTypeId'=>$userTypeId,'userType'=>$userType['user_type'], );

                return view('admin.user-privilege.form')
                    ->with('entity',$inputs)
                    ->with('privileges',$userPrivileges);

        }else{

                session()->flush();
                Auth::logout();
                session()->forget('privileges');
                return view('admin.login');

        }

    }


    public function store(){
//dd(Input::all());
        $entityId = Input::get('userTypeId');
//        $description = Input::get('description');
        $description = null;
        $privileges = Input::get('privilegeId');


        $rules = array();

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) { //echo "validator if"; exit;

            return Redirect::back()
                ->withErrors($validator)
                ->withInput();

        } else { //echo "validator else"; exit;

            $this->user_privilege->deleteUserPrivilages($entityId);
            if(sizeof($privileges) >= 1){

                foreach($privileges as $privilege){

                    $inputs = array('privilege_id'=>$privilege,'user_type_id'=>$entityId,'description'=>$description);
                    $this->user_privilege->create($inputs);
                }

            }

            return Redirect::to('/admin/user/privileges')->with('message', 'Privilege Updated successfully.');

        }
    }

//    public function create($id = NULL){
//
//        $privileges = $this->user_type->getAllPrivileges(1);
//
//        $types = $this->user_type->getAllUserType(1);
//        $userTypes = array();
//        $userTypes[null] = 'Select User Type';
//        foreach($types as $type){
//
//            $userTypes[$type->id] = $type->user_type;
//        }
//
////        $categoriesArray = $this->category->getAll(1);
////        $categories = array();
////        $categories[null] = 'Select Categories';
////        foreach($categoriesArray as $category){
////
////            $categories[$category->id] = $category->category_name;
////        }
////        dd($departments);
//
//        $entity = null;
//
//        if($id != NULL){
//
//            $entity = $this->user_type->getById($id);
//        }
//
//        return view('admin.user-privilege.form')
//            ->with('userTypes',$userTypes)
//            ->with('privileges',$privileges)
//            ->with('entity',$entity);
//
//    }
}
