<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\User;
use Auth;

//use App\Binding\User\UserRepository as UserRepository;

class AdminHomeController extends Controller {

	public function __construct()
	{              
        //    $this->middleware('adminAuth');

		//$this->user = $user;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$isSuperAdmin = strstr(Auth::user()->role,'1');
		$articleMenu = 0;
		$categoryMenu = 0;
		$departmentMenu = 0;
		$userMenu = 0;

		return view('admin.home');

	}
}
