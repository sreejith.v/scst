<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\User;
use App\OfficeType;
use App\District;
use App\Office;

class OfficeController extends Controller
{
    //
    public function __construct(){

    }

    public function index($id = NULL){

        $entities = DB::table('office')->get();
        return view('admin.office.lists')->with('entities',$entities);
    }

    public function create($id = NULL){

        $id = base64_decode($id);

        $office_Types = DB::table('officeType')->get();

        $districts = DB::table('districts')->get();

        $entity = null;
        if($id != NULL){
            $entity = Office::where('id','=',$id)->first();
            $officetype_id = $entity['officeType_id'];
            $district_id = $entity['district_id'];
        }
        else {
            $officetype_id = '';
        }

        return view('admin.office.form')->with('entity',$entity)->with('office_Types',$office_Types)->with('districts',$districts)->with('officetype_id',$officetype_id)->with('district_id',$district_id);
    }

    public function store(){

        $entityId = Input::get('entityId');

        $office_name = Input::get('name');

        $officeType_id = Input::get('officeType_id');

        $district_id = Input::get('district_id');

        if($officeType_id == 'select')
        {
            return Redirect::back()->withInput()->withErrors("Please select the office type and proceed.");
        }

        if($district_id == 'select')
        {
            return Redirect::back()->withInput()->withErrors("Please select the district and proceed.");
        }


        $rules = array(
            'name' => 'required',
            'officeType_id' => 'required',
            'district_id' => 'required',
            'pincode' => 'max:6',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) { //echo "validator if"; exit;

            return Redirect::back()
                ->withErrors($validator)
                ->withInput();

        } else { //echo "validator else"; exit;

            $input = Input::all();

            if (is_null($entityId))
            {
                $result = Office::create($input);
                //$this->departments->create(Input::all());
                return Redirect::to('/admin/offices')->with('message', 'Office added successfully.');
            }
            else
            {
                Office::where('id', $entityId)
                    ->update(array('name' => $input['name'],'officeType_id' => $input['officeType_id'],'district_id' => $input['district_id'],
                        'code' => $input['code'], 'email' => $input['email'], 'phone' => $input['phone'],'pincode' => $input['pincode'] ));
                return Redirect::to('/admin/offices')->with('message', 'Office updated successfully.');
            }
        }
    }

    public function delete($id)
    {

        $id = base64_decode($id);

        Office::where('id','=',$id)->delete();

        return Redirect::to('/admin/offices')->with('message', 'Office Deleted successfully.');
    }


    public function changeStatus($id,$status){

        $id = base64_decode($id);
        $status = base64_decode($status);

        if($status == 1){
            $this->departments->activate($id,$status);
            $msg = 'Department Activated';
        }else{
            $this->departments->deactivate($id,$status);
            $msg = 'Department De-Activated';
        }

        return Redirect::to('/admin/departments')->with('message', $msg);
    }


}
