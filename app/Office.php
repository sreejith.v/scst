<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    //
   protected $table = "office";

   protected $fillable = [
       'district_id',
       'officeType_id',
       'name',
       'code',
       'email',
       'phone',
       'pincode',
       'status',
       'created_at',
       'updated_at'
   ];
}
