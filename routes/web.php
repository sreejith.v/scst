<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(array('prefix'=>'admin'),function() {
    Route::get('/', 'Admin\AdminLoginController@index');
    Route::get('login', 'Admin\AdminLoginController@index');
    Route::post('check', 'Admin\AdminLoginController@checkLogin');
    Route::get('logout', 'Admin\AdminLoginController@logout');
    Route::get('dashboard', 'Admin\AdminHomeController@index');

    Route::get('offices', array('uses' => 'Admin\OfficeController@index','as'=>'admin_office_index'));
    Route::get('/office/create', array('uses' => 'Admin\OfficeController@create','as'=>'admin_office_create'));
    Route::post('/office/store',array('uses' => 'Admin\OfficeController@store','as'=>'admin_office_store'));
    Route::get('/office/update/{id}',array('uses' => 'Admin\OfficeController@create','as'=>'admin_office_update'));
    Route::get('/office/delete/{id}',array('uses' => 'Admin\OfficeController@delete','as'=>'admin_office_delete'));
    Route::get('/office/status/{id}/{status}',array('uses' => 'Admin\OfficeController@changeStatus','as'=>'admin_office_status'));

});
