<?php echo
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    {{-- <title>Admin Dashboard | M Governence</title> --}}
    <title>{{ Config::get('constants.SITE_NAME') }}</title>
    <!-- Favicons-->
    <link rel="icon" href="{{ asset('/template/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/template/images/favicon/apple-touch-icon-152x152.png') }}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->

    <!-- CORE CSS-->
    <link href="{{ asset('/template/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/template/css/style.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/template/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="{{ asset('/template/css/custom/custom.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('/template/js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/template/js/plugins/jvectormap/jquery-jvectormap.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/template/js/plugins/chartist-js/chartist.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    <link href="{{ asset('/template/js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    <link href="{{ asset('/template/js/plugins/prism/prism.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/template/js/plugins/data-tables/css/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/css/custom.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    <!--jsgrid css-->
    <link href="{{ asset('/template/js/plugins/jsgrid/css/jsgrid.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/template/js/plugins/jsgrid/css/jsgrid-theme.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    <link href="{{ asset('/template/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

    @yield('script')

</head>

<body>

<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<!-- START HEADER -->

<header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
        <nav class="navbar-color">
            <div class="nav-wrapper">
                <a class="navbar-brand" ><b>SC ST</b> Development Corporation</a>
                {{--<ul class="left">--}}
                    {{--<li><h1 class="logo-wrapper"><a href="{{ url('/admin/dashboard') }}" class="brand-logo darken-1"><img src="{{ asset('/template/images/logo_kerala.png') }}" alt="M Keralam logo" style="max-height: 37px; width: auto"></a> <span class="logo-text"></span></h1></li>--}}
                {{--</ul>--}}
            </div>
        </nav>
    </div>
</header>

<!-- START MAIN -->
<div id="main">

    <div class="wrapper">

         @include('admin.left-menubar')

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->

        @yield('content')

        <!-- END CONTENT -->

        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START RIGHT SIDEBAR NAV-->
        <aside id="right-sidebar-nav">
            <ul id="chat-out" class="side-nav rightside-navigation">
                <li class="li-hover">
                    <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
                    <div id="right-search" class="row">
                        <form class="col s12">
                            <div class="input-field">
                                <i class="mdi-action-search prefix"></i>
                                <input id="icon_prefix" type="text" class="validate">
                                <label for="icon_prefix">Search</label>
                            </div>
                        </form>
                    </div>
                </li>
                <li class="li-hover">
                    <ul class="chat-collapsible" data-collapsible="expandable">
                        <li>
                            <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                            <div class="collapsible-body recent-activity">
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">just now</a>
                                        <p>Jim Doe Purchased new equipments for zonal office.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Yesterday</a>
                                        <p>Your Next flight for USA will be on 15th August 2015.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">5 Days Ago</a>
                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Last Week</a>
                                        <p>Jessy Jay open a new store at S.G Road.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">5 Days Ago</a>
                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                            <div class="collapsible-body sales-repoart">
                                <div class="sales-repoart-list  chat-out-list row">
                                    <div class="col s8">Target Salse</div>
                                    <div class="col s4"><span id="sales-line-1"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Payment Due</div>
                                    <div class="col s4"><span id="sales-bar-1"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Total Delivery</div>
                                    <div class="col s4"><span id="sales-line-2"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Total Progress</div>
                                    <div class="col s4"><span id="sales-bar-2"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                            <div class="collapsible-body favorite-associates">
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="{{ asset('/template/images/avatar.jpg') }}" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Eileen Sideways</p>
                                        <p class="place">Los Angeles, CA</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="{{ asset('/template/images/avatar.jpg') }}" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Zaham Sindil</p>
                                        <p class="place">San Francisco, CA</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="{{ asset('/template/images/avatar.jpg') }}" alt="" class="circle responsive-img offline-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Renov Leongal</p>
                                        <p class="place">Cebu City, Philippines</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="{{ asset('/template/images/avatar.jpg') }}" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Weno Carasbong</p>
                                        <p>Tokyo, Japan</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="{{ asset('/template/images/avatar.jpg') }}" alt="" class="circle responsive-img offline-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Nusja Nawancali</p>
                                        <p class="place">Bangkok, Thailand</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </aside>
        <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>

</div>
<!-- END MAIN -->

<!-- START FOOTER -->
<footer class="page-footer">
</footer>
<!-- END FOOTER -->

<!-- jQuery Library -->
<script type="text/javascript" src="{{ asset('/template/js/plugins/jquery-1.11.2.min.js') }}"></script>
<!--materialize js-->
<script type="text/javascript" src="{{ asset('/template/js/materialize.min.js') }}"></script>
<!--scrollbar-->
<script type="text/javascript" src="{{ asset('/template/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

<!--prism-->
<script type="text/javascript" src="{{ asset('/template/js/plugins/prism/prism.js') }}"></script>

<!--angularjs-->
<script type="text/javascript" src="{{ asset('/template/js/plugins/angular.min.js') }}"></script>

<!-- data-tables -->
<script type="text/javascript" src="{{ asset('/template/js/plugins/data-tables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/template/js/plugins/data-tables/data-tables-script.js') }}"></script>

<!-- chartist -->
{{--<script type="text/javascript" src="{{ asset('/template/js/plugins/chartist-js/chartist.min.js') }}"></script>--}}

<!-- chartjs -->
{{--<script type="text/javascript" src="{{ asset('/template/js/plugins/chartjs/chart.min.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ asset('/template/js/plugins/chartjs/chart-script.js') }}"></script>--}}

<!-- sparkline -->
<script type="text/javascript" src="{{ asset('/template/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/template/js/plugins/sparkline/sparkline-script.js') }}"></script>

<!-- google map api -->
{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAZnaZBXLqNBRXjd-82km_NO7GUItyKek"></script>--}}

<!--jvectormap-->
<script type="text/javascript" src="{{ asset('/template/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/template/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script type="text/javascript" src="{{ asset('/template/js/plugins/jvectormap/vectormap-script.js') }}"></script>

<!--google map-->
{{--<script type="text/javascript" src="{{ asset('/template/js/plugins/google-map/google-map-script.js') }}"></script>--}}

<!--jsgrid-->
<script type="text/javascript" src="{{ asset('/template/js/plugins/jsgrid/js/db.js') }}"></script> <!--data-->
<script type="text/javascript" src="{{ asset('/template/js/plugins/jsgrid/js/jsgrid.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/template/js/plugins/jsgrid/js/jsgrid-script.js') }}"></script>



<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="{{ asset('/template/js/plugins.min.js') }}"></script>
<!--custom-script.js - Add your own theme custom JS-->
<script type="text/javascript" src="{{ asset('/template/js/custom-script.js') }}"></script>
<!-- Toast Notification -->

<script type="text/javascript">
    // Toast Notification
    $(window).load(function() {
        setTimeout(function() {
       //     Materialize.toast('<span>Hiya! I am a toast.</span>', 1500);
        }, 1500);
        setTimeout(function() {
        //    Materialize.toast('<span>You can swipe me too!</span>', 3000);
        }, 5000);
        setTimeout(function() {
        //    Materialize.toast('<span>You have new order.</span><a class="btn-flat yellow-text" href="#">Read<a>', 3000);
        }, 15000);
    });
</script>

<script type="text/javascript">
    /*Show entries on click hide*/
    $(document).ready(function(){
        $(".dropdown-content.select-dropdown li").on( "click", function() {
            var that = this;
            setTimeout(function(){
                if($(that).parent().hasClass('active')){
                    $(that).parent().removeClass('active');
                    $(that).parent().hide();
                }
            },100);
        });
    });
</script>

</body>

</html>