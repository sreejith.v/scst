<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation">

        @if ((Request::is('admin/dashboard')))
            <li class="bold active" ><a href="{{ url('/admin/dashboard') }}" class="waves-effect waves-cyan"><i
                            class="mdi-action-dashboard"></i> Dashboard</a>
            </li>
        @else
            <li class="bold"><a href="{{ url('/admin/dashboard') }}" class="waves-effect waves-cyan"><i
                            class="mdi-action-dashboard"></i> Dashboard</a>
            </li>
        @endif

        <?php

        //Checking that logged user is super admin or not. Based on user role field
        $isSuperAdmin = strstr(Auth::user()->role, '1');

        ?>

                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">

                            <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i
                                            class="mdi-device-now-widgets"></i> Offices</a>
                                <div class="collapsible-body">
                                    <ul>
                                            <li><a href="{{ url('/admin/office/create') }}">Add Office</a>
                                            </li>
                                            <li><a href="{{ url('/admin/offices') }}">View Office</a></li>
                                    </ul>
                                </div>
                            </li>
                    </ul>
                </li>

                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">

                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i
                                        class="mdi-device-now-widgets"></i> Staff</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="{{ url('/admin/staff/create') }}">Add Staff</a>
                                    </li>
                                    <li><a href="{{ url('/admin/staff') }}">View Staff</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>

                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">

                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i
                                        class="mdi-device-now-widgets"></i> Applications</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="{{ url('/admin/staff/create') }}">List Applications</a>
                                    </li>
                                    <li><a href="{{ url('/admin/staff') }}">List Applications</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>

                <li class="bold"><a class="collapsible-header waves-effect waves-cyan"
                                    href="{{ url('/admin/logout') }}">
                        <i class="mdi-av-queue"></i> Logout </a>
                </li>

        <li class="li-hover">
            <div class="divider"></div>
        </li>

    </ul>
    <a href="#" data-activates="slide-out"
       class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i
                class="mdi-navigation-menu"></i></a>
</aside>
<!-- END LEFT SIDEBAR NAV-->