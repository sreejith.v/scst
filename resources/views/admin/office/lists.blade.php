
@extends('admin/app-home')
@section('content')

    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <section id="content">

        <!--start container-->
        <div class="container">

            <!--card stats start-->
            <div class="container">
                <div class="section">

                    <!--DataTables example-->
                    <div id="table-datatables">
                        <h4 class="header">Department List</h4>
                        <div class="row">

                            <div class="col s12 m8 l9">
                                <table id="data-table-simple" class="responsive-table display" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Sl No.</th>
                                        <th>Office Name</th>
                                        <th>Office Code</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($entities as $key=>$entity)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$entity->name}}</td>
                                            <td>{{$entity->code}}</td>
                                            <td>{{$entity->email}}</td>
                                            <td>
                                                <a href="{{'office/update/'.base64_encode($entity->id)}}"><i class="mdi-editor-mode-edit" style="font-size: 19px;" title="Edit"></i></a> &nbsp;&nbsp;&nbsp;
                                                <a onClick="return confirmDelete();" href="{{'office/delete/'.base64_encode($entity->id)}}"> <i class="mdi-action-delete"  title="Delete" style="font-size: 19px;"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="divider"></div>
                    <!--DataTables example Row grouping-->

                </div>

            </div>

            <div id="card-widgets">
                <div class="row">

                </div>

                <div class="row">

                    <!-- map-card -->
                    <div class="col s12 m12 l4" style="display: none;">
                        <div class="map-card">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <div id="map-canvas" data-lat="40.747688" data-lng="-74.004142"></div>
                                </div>
                                <div class="card-content">
                                    <a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right">
                                        <i class="mdi-maps-pin-drop"></i>
                                    </a>
                                    <h4 class="card-title grey-text text-darken-4"><a href="#" class="grey-text text-darken-4">Company Name LLC</a>
                                    </h4>
                                    <p class="blog-post-content">Some more information about this company.</p>
                                </div>
                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4">Company Name LLC <i class="mdi-navigation-close right"></i></span>
                                    <p>Here is some more information about this company. As a creative studio we believe no client is too big nor too small to work with us to obtain good advantage.By combining the creativity of artists with the precision of engineers we develop custom solutions that achieve results.Some more information about this company.</p>
                                    <p><i class="mdi-action-perm-identity cyan-text text-darken-2"></i> Manager Name</p>
                                    <p><i class="mdi-communication-business cyan-text text-darken-2"></i> 125, ABC Street, New Yourk, USA</p>
                                    <p><i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i> +1 (612) 222 8989</p>
                                    <p><i class="mdi-communication-email cyan-text text-darken-2"></i> support@geekslabs.com</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <!--end container-->
        </div>
    </section>

    <script type="text/JavaScript">

        function confirmDelete(){
            var agree=confirm("Are you sure you want to delete this...?");
            if (agree)
                return true ;
            else
                return false ;
        }

    </script>
@endsection