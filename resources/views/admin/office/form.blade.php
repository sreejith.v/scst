
@extends('admin/app-home')
@section('content')

    <section id="content">

        <!--start container-->
        <div class="container">

            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">@if(sizeof($entity) >= 1)Update Office @else Add Office @endif </h3>
                        </div>
                        <div class="panel-body form_view">

                            {!! Form::model($entity, array('url' => URL::route('admin_office_store'),'enctype' => 'multipart/form-data','method' => 'POST','class'=>"formular form-horizontal ls_form")) !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if($entity != NULL)
                                @if($entity->password != '' )
                                    <input type="hidden" name="old_password" value="{{ $entity->password }}">
                                @endif
                            @endif

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <div class="row ls_divider">

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Office Name</label>
                                    <div class="col-md-6">
                                        {!! Form::text('name',Input::old('name'), array('class' => 'form-control text-input','required' => 'required')) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Office Type</label>
                                    <div class="col-md-6">
                                        <select name="officeType_id" id="officeType_id" class="span6" value="{{ old('officeType_id') }}">
                                            <option value="select">Select</option>
                                            @foreach($office_Types as  $office_Types)
                                                <option value="{{ $office_Types->id }}" {{ $officetype_id == $office_Types->id ? 'selected="selected"' : '' }}>{{ $office_Types->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">District</label>
                                    <div class="col-md-6">
                                        <select name="district_id" id="district_id" class="span6" value="{{ old('district_id') }}">
                                            <option value="select">Select</option>
                                            @foreach($districts as  $districts)
                                                <option value="{{ $districts->id }}" {{ $district_id == $districts->id ? 'selected="selected"' : '' }}>{{ $districts->district }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Office Code</label>
                                    <div class="col-md-6">
                                        {!! Form::text('code',Input::old('code'), array('class' => 'form-control text-input','required' => 'required')) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Email</label>
                                    <div class="col-md-6">
                                        {!! Form::text('email',Input::old('email'), array('class' => 'form-control text-input','required' => 'required')) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Phone</label>
                                    <div class="col-md-6">
                                        {!! Form::text('phone',Input::old('phone'), array('class' => 'form-control text-input','required' => 'required')) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Pincode</label>
                                    <div class="col-md-6">
                                        {!! Form::text('pincode',Input::old('pincode'), array('class' => 'form-control text-input','required' => 'required')) !!}
                                    </div>
                                </div>


                            </div>

                            <div class="row ls_divider last">
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-sm-10">

                                        {{----------------------- For Edit Get Id of the entity -------}}

                                        @if($entity != NULL)
                                            {!! Form::hidden('entityId', $entity->id, array('name' => 'entityId')) !!}
                                        @endif

                                        <button class="submit ls-yellow-btn btn" type="submit" name="submit">Save</button>
                                        <a class="submit btn ls-brown-btn " href="{{ URL::previous() }}">Cancel</a>
                                    </div>
                                </div>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>

            <div id="card-widgets">
                <div class="row">

                </div>

                <div class="row">

                    <!-- map-card -->
                    <div class="col s12 m12 l4" style="display: none;">
                        <div class="map-card">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <div id="map-canvas" data-lat="40.747688" data-lng="-74.004142"></div>
                                </div>
                                <div class="card-content">
                                    <a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right">
                                        <i class="mdi-maps-pin-drop"></i>
                                    </a>
                                    <h4 class="card-title grey-text text-darken-4"><a href="#" class="grey-text text-darken-4">Company Name LLC</a>
                                    </h4>
                                    <p class="blog-post-content">Some more information about this company.</p>
                                </div>
                                <div class="card-reveal">
                                    <span class="card-title grey-text text-darken-4">Company Name LLC <i class="mdi-navigation-close right"></i></span>
                                    <p>Here is some more information about this company. As a creative studio we believe no client is too big nor too small to work with us to obtain good advantage.By combining the creativity of artists with the precision of engineers we develop custom solutions that achieve results.Some more information about this company.</p>
                                    <p><i class="mdi-action-perm-identity cyan-text text-darken-2"></i> Manager Name</p>
                                    <p><i class="mdi-communication-business cyan-text text-darken-2"></i> 125, ABC Street, New Yourk, USA</p>
                                    <p><i class="mdi-action-perm-phone-msg cyan-text text-darken-2"></i> +1 (612) 222 8989</p>
                                    <p><i class="mdi-communication-email cyan-text text-darken-2"></i> support@geekslabs.com</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

@endsection