<?php echo
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Admin Login | SC ST Department</title>

    <!-- Favicons-->
    <link rel="icon" href="{{ asset('/template/images/favicon/favicon-32x32.png') }}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/template/images/favicon/apple-touch-icon-152x152.png') }}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="{{ asset('/template/images/favicon/mstile-144x144.png') }}">
    <!-- For Windows Phone -->

    <!-- CORE CSS-->

    <link href="{{ asset('/template/css/materialize.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/template/css/style.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="{{ asset('/template/css/custom/custom.min.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/template/css/layouts/page-center.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{ asset('/template/js/plugins/prism/prism.css') }}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{ asset('/template/js/plugins/perfect-scrollbar/perfect-scrollbar.css') }}" type="text/css" rel="stylesheet" media="screen,projection">

</head>

<body class="cyan">
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->



<div id="login-page" class="row">
    <div class="col s12 z-depth-4 card-panel">
        {!! Form::open(array('url' => 'admin/check', 'id' => 'login','class'    =>  'form-signin')) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        @if ($errors->has('error'))<span id="reauth-email" class="reauth-email"><font color="red">{!!$errors->first('error')!!}</font></span>@endif
            <div class="row">
                <div class="input-field col s12 center">
                    <img src="{{ asset('/template/images/logo_kerala.png') }}" alt="" class="responsive-img valign profile-image-login">
                    <p class="center login-form-text">SC ST Department Admin Login</p>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-communication-email prefix"></i>
                    <input type="email" name="username" autocomplete="off" id="inputEmail" class="form-control" placeholder="Email address" value="{{ Request::old('username') }}" required autofocus><input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="mdi-action-lock-outline prefix"></i>
                    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" value="{{ Request::old('password') }}" required>
                </div>
            </div>
        {{--
            <div class="row">
                <div class="input-field col s12 m12 l12  login-text">
                    <input type="checkbox" id="remember-me" />
                    <label for="remember-me">Remember me</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <a href="index.html" class="btn waves-effect waves-light col s12">Login</a>
                </div>
            </div>
        --}}

            <div class="row">
                <div class="input-field col s12">
                    <button class="btn waves-effect waves-light col s12" type="submit">Sign in</button>
                </div>
            </div>


        {!! Form::close() !!}



    </div>
</div>

<div class="input-field col s12">
    Copyright © {{ date('Y') }} IT Mission. All Rights Reserved
</div>

<!-- jQuery Library -->
<script type="text/javascript" src="{{ asset('/template/js/plugins/jquery-1.11.2.min.js') }}"></script>
<!--materialize js-->
<script type="text/javascript" src="{{ asset('/template/js/materialize.min.js') }}"></script>
<!--prism-->
<script type="text/javascript" src="{{ asset('/template/js/plugins/prism/prism.js') }}"></script>
<!--scrollbar-->
<script type="text/javascript" src="{{ asset('/template/js/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>

<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="{{ asset('/template/js/plugins.min.js') }}"></script>
<!--custom-script.js - Add your own theme custom JS-->
<script type="text/javascript" src="{{ asset('/template/js/custom-script.js') }}"></script>

</body>

</html>