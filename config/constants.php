<?php
//file : app/config/constants.php

return [
    'SITE_NAME' => 'Admin Dashboard | SC ST Development Department',
    'COPYRIGHT_YEAR' => date('Y'),
];